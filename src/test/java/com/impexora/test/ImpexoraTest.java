/**
 * 
 */
package com.impexora.test;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.impexora.commons.CommonConstants;
import com.impexora.model.ResponseWrapper;
import com.impexora.ws.ImpexoraWebService;

/**
 * @author ashraf_sarhan
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/applicationContext.xml")
public class ImpexoraTest extends TestCase {

	@Autowired
	private ImpexoraWebService impexoraWebService;

	@Test
	public void testImpexoraStatus() throws IOException {
//		List<SolrHost> solrHosts = new ArrayList<SolrHost>();
//
//		SolrHost solrHost = new SolrHost();
//		solrHost.setHost("localhost");
//		solrHost.setPort(8080);
//		solrHost.setCore("avago_xref");
//		solrHosts.add(solrHost);
//
//		SolrHost solrHost2 = new SolrHost();
//		solrHost2.setHost("10.68.22.76");
//		solrHost2.setPort(8090);
//		solrHost2.setCore("cyp");
//		solrHosts.add(solrHost2);
//		
//		List<Response> responses = new ArrayList<Response>();
//
//		for (SolrHost sh : solrHosts) {
//			responses.add(AsyncHttpRequest.executeReq(sh,
//					CommonConstants.STATUS));
//		}
//
//		for (Response rs : responses) {
//			System.out.println("Response: " + rs.getResponseBody() + "\n");
//		}
//
//		Assert.assertNotNull("Failure in executing AsyncHttpRequest", responses);
//
//		Assert.assertEquals("Failure in executing AsyncHttpRequest", 2,
//				responses.size());
		
		String command = CommonConstants.STATUS;
		
		String solr = "[{\"host\":\"10.68.22.56\", \"port\":8080 , \"core\":\"avago_xref\"},{\"host\":\"10.68.22.76\", \"port\":8090 , \"core\":\"cyp\"}]";
		
		MockHttpServletResponse httpServletResponse = new MockHttpServletResponse(); 
		
		ResponseWrapper responseWrapper = impexoraWebService.impexer(command, solr, null, httpServletResponse);
		
		System.out.println("Response Status: " + responseWrapper.getMessage() + "\n");
		
		System.out.println("Response Code: " + httpServletResponse.getStatus());
		
		Assert.assertNotNull("%%%%%% Failure in executing solr status AsyncHttpRequest %%%%%%", responseWrapper);
		
		Assert.assertEquals("%%%%%% Failure in executing solr status AsyncHttpRequest %%%%%%", 200, httpServletResponse.getStatus());
	
	}

}
